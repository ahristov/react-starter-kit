# React Starter Kit

## Description

This starter kit creates simple modular infrastructure for developing React component apps and libraries and combines:

- webpack

- muicss

- stylus


## Usage

To use the kit follow the steps below.

1. Install the following packages globally

Install these packages:

    npm install bower -g
    npm install webpack -g
    npm install webpack-dev-server -g
    npm install babel-cli -g
    npm install jsx-loader -g
    npm install babel-loader -g

1. Run from this directory

    npm install

1. Run webpack dev serve


Start serve the project with hot reload run:

    webpack-dev-server --content-base build/

, or if you get javascript error:

    Uncaught Error: [HMR] Hot Module Replacement is disabled.

, then use:

    webpack-dev-server --hot --inline --content-base build/


Open the url: http://localhost:8080/



## Notes

Side note on `babel`: To enable babel support these are the different ways:

- Per project: npm install babel-loader --save-dev

- In your home directory: cd $HOME ; npm install babel-loader

- Globally: npm install -g babel-loader


## Links

[WebPack Plugin for Babel](https://github.com/babel/babel-loader)

[Babel and ES6](http://www.2ality.com/2015/04/webpack-es6.html)

[ReactJS on ES6](https://babeljs.io/blog/2015/06/07/react-on-es6-plus)

[ReactJS and WebPack Getting Started](http://humaan.com/getting-started-with-webpack-and-react-es6-style/)

[Using external React](https://github.com/gaearon/react-hot-loader/blob/master/docs/README.md#usage-with-external-react)

[Refactoring React to ES6 Classes](http://www.newmediacampaigns.com/blog/refactoring-react-components-to-es6-classes)