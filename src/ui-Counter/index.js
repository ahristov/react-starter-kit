import './index.styl';

import { Button } from 'muicss/react';
import React, { Component } from 'react';

export default class Counter extends Component {
    
    constructor() {
        super();
        this._handleClick = this._handleClick.bind(this);
        this.state = {
            counter: 0
        };
    }
    
    //getInitialState() 
    //    return { counter: 0 };
    //}

    _handleClick() {
        this.setState({ counter: this.state.counter+1 });
    }
    
    render() {
		return (
            <div className="ui-panel">
                <h1>Counter: {this.state.counter}</h1>
                <Button variant="raised" color="primary" onClick={this._handleClick}>Increment</Button>
            </div>
		);
	}
};
    

