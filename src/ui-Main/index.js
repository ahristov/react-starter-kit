import './index.styl';

import { Button } from 'muicss/react';
import React, { Component } from 'react';
import Counter from '../ui-Counter';


export default class Main extends Component {
	render() {
		return (
			<div className="ui-container">
				<div className="ui-panel">
					<h1>React Components Example</h1>
					<Button variant="raised" color="primary">button</Button>
					<button className="ui-btn ui-btn-primary ui-btn-raised">Continue</button>
                    <button className="ui-btn ui-btn-primary">Continue</button>
				</div>
            
                <Counter/>
			</div>
		);
	}
};

